const Building = require('../models/admin/Buildings');

module.exports.AddStorage = async (req, res) => {
	try {

		let buildingId = req.params.buildingId;
		let { storageName } = req.body;

		const storage = await Building.findOne({ _id: buildingId, "samplesStorage.storageName": storageName });

		if (storage === null) {
			const updateBuilding = await Building.findOneAndUpdate(
				{ _id: buildingId },
				{ $push: { samplesStorage: { storageName } } },
				{ new: true }
			)
			res.send(true);
		} else {
			res.send(false)
		}

	} catch (error) {
		console.error(error);
		res.send(false)
	}
	
}

module.exports.RemoveStorage = async (req, res) => {
  try {
    const { storageId, buildingId } = req.params;
    const storage = await Building.findOneAndUpdate(
      { _id: buildingId, "samplesStorage._id": storageId },
      { $pull: { samplesStorage: { _id: storageId } } },
      { new: true }
    );

    if (!storage) {
      res.status(404).send('Not found');
      return;
    }

    res.send(true); // Signal successful removal
  } catch (error) {
    console.error(error);
    res.status(500).send(false); // Indicate error
  }
};

module.exports.UpdateStorage = async (req, res) => {

	try {
		const updated = { storageName: req.body.newStorageName };
		const { storageId, buildingId } = req.params;

		await Building.updateOne(
			{ _id: buildingId, "samplesStorage._id": storageId },
			{ $set: { "samplesStorage.$.storageName": updated.storageName } },
			{ new: true }
		)

			res.send(true);

	} catch(error) {
			console.error(error)
			res.send(false)
	}
}