const Users = require('../models/Users.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');
const Building = require('../models/admin/Buildings');

/*
User Roles
0 - administrator
1 - Lab Manager
*/

module.exports.RegisterUser = (request, response) => {
	Users.findOne({ email : request.body.email })
	.then(result => {
		if(result === null) {
			let newUser = new Users({
				name: request.body.name,
				email : request.body.email,
				imageUrl: request.body.imageUrl,
				password : bcrypt.hashSync(request.body.password, 10),
				userRole: request.body.userRole,
				buildingsAssigned: request.body.buildingsAssigned
			})
			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false))
		} else {
			response.send(result)
		}
	})
	.catch(err => response.send(false))
}

module.exports.Login = (request, response) => {

	Users.findOne({ email : request.body.email })
	.then(result => {
	  if(!result) {
	    return response.send(false)
	  } else {
	    const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

	    if(isPasswordCorrect){  
	      return response.send({auth: auth.createAccessToken(result)})
	    } else {
	      return response.send(false);
	    } 
	  }
	})
	.catch(error => {
	  console.log('error:', error);
	  response.send(false);
	});
}

module.exports.GetUserDetails = (request, response) => {		
		const userData = auth.decode(request.headers.authorization);
		if (userData.userRole === 0) {
			Users.findById(request.params.userId)
			.then(result => response.send(result))
			.catch(error => response.send(false))
		} else {
			return response.send(false);
		}
}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	Users.findOne({_id : userData.id })
	.then(data => response.send(data));
}

module.exports.retrieveUser = (request, response) => {

	let userId = request.params.userId;

	Users.findById(userId)
	.then(result => {
		return response.send(result)
	})
	.catch(error => response.send(false))
}

// module.exports.retrieveUser = (request, response) => {
//   const userId = request.params.userId;

//   // Query for the user in the Users collection
//   Users.findById(userId)
//     .then(result => {
//       // If user is found:
//       if (result) {
//         // Check for existence in Building.usersList
//         return Building.findOne({ _id: result.buildingId }) // Assuming a buildingId link
//           .then(building => {
//             if (building && building.usersList.includes(userId)) {
//               return response.send(false); // User already exists in building's list
//             } else {
//               return response.send(result); // User not in building's list, proceed
//             }
//           })
//           .catch(error => response.send(false)); // Handle errors in building lookup
//       } else {
//         return response.send(false); // User not found in Users collection
//       }
//     })
//     .catch(error => response.send(false)); // Handle errors in user lookup
// };


module.exports.retrieveUsersList = (req, res) => {
    Users.find({ userRole: { $in: [1, 2] } })
        .then(response => {
            if (response.length !== 0) {
                return res.send(response);
            } else {
                res.send(false);
            }
        })
        .catch(error => {
            // Handle error appropriately
            console.error(error);
            res.status(500).send('Internal Server Error');
        });
};

module.exports.DeleteUser = async (req, res) => {
  try {
    const userId = req.params.userId;

    // Delete user from Users collection
    await Users.findByIdAndDelete(userId);

    // Remove user from buildings' usersList if they exist
    await Building.updateMany(
      { "usersList.userId": userId },
      { $pull: { usersList: { userId } } }
    );

    // User deleted successfully, regardless of existence in buildings
    res.send(true);
  } catch (error) {
    console.error(error);
    if (error.name === 'NotFound') {
      res.status(404).send(false); // User not found in Users collection
    } else {
      res.status(500).send(false); // Internal server error
    }
  }
};


module.exports.UpdateUser = async (req, res) => {
  try {
    const userId = req.params.userId;
    const updatedUser = {
      name: req.body.newName,
      email: req.body.newEmail,
      imageUrl: req.body.newImageUrl
    };

    // Update user in Users collection
    const user = await Users.findByIdAndUpdate(userId, updatedUser, { new: true });

    if (!user) {
      return res.status(404).send(false); // User not found
    }

    // Update user in buildings' usersList
    const updatedBuildings = await Building.updateMany(
      { "usersList.userId": userId },
      { $set: { "usersList.$.name": updatedUser.name, "usersList.$.email": updatedUser.email, "usersList.$.imageUrl": updatedUser.imageUrl } }
    );

    res.send(true);
  } catch (error) {
    console.error(error);
    res.status(500).send(false); // Internal server error
  }
};


module.exports.SetStatus = (req, res) => {
	const userId = req.params.userId;
	let updatedStatus = {
		isActive: req.body.isActive
	}

	Users.findByIdAndUpdate(userId, updatedStatus)
	.then(result => res.send(true))
	.catch(error => res.send(false))
}

module.exports.ChangeUserRole = (request, response) => {
	const userId = request.params.userId;
	const updatedUserRole = {
		userRole: request.body.newUserRole
	}

	Users.findByIdAndUpdate(userId, updatedUserRole)
	.then(result => response.send(true))
	.catch(error => response.send(false))

}
