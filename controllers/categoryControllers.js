const Category = require('../models/Categories');
const Building = require('../models/admin/Buildings');

module.exports.AddCategory = async (req, res) => {
	try {

		let buildingId = req.params.buildingId;
		let { categoryName, imageUrl } = req.body;

		const category = await Building.findOne({ _id: buildingId, "categories.categoryName": categoryName });

		if (category === null) {

			 await Building.findOneAndUpdate(
				{ _id: buildingId },
				{ $push: { categories: { categoryName, imageUrl } } },
				{ new: true }
			)
			res.send(true);
		} else {
			res.send(false)
		}

	} catch (error) {
		console.error(error);
		res.send(false)
	}
	
} 


module.exports.UpdateCategory = async (req, res) => {
  try {
    let updatedCategory = {
      categoryName: req.body.newCategoryName,
      imageUrl: req.body.newImageUrl,
    };

    let categoryId = req.params.categoryId;
    let buildingId = req.params.buildingId;

    await Building.updateOne(
      { _id: buildingId, "categories._id": categoryId },
      { $set: { "categories.$.categoryName": updatedCategory.categoryName, "categories.$.imageUrl": updatedCategory.imageUrl } }
    );

    res.send(true);
  } catch (error) {
    console.error(error);
    res.send(false);
  }
};


module.exports.RemoveCategory = async (req, res) => {
  try {
    const categoryId = req.params.categoryId;
    let buildingId = req.params.buildingId;

    const building = await Building.findOneAndUpdate(
      { _id: buildingId, "categories._id": categoryId },
      { $pull: { categories: { _id: categoryId } } },
      { new: true }
    );

    if (!building) {
      res.status(404).send('Building not found');
      return;
    }

    res.send(true); // Signal successful removal
  } catch (error) {
    console.error(error);
    res.status(500).send(false); // Indicate error
  }
};

module.exports.RetrieveCategories = (req, res) => {
    Category.find({ buildingID: req.params.buildingId })
    .then(result => res.send(result))
    .catch(error => res.send(false));
 }
