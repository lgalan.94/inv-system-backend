const Building = require('../models/admin/Buildings');

module.exports.AddSample = async (req, res) => {
	try {

		let buildingId = req.params.buildingId;
		let { sampleName, description, location } = req.body;

		const sample = await Building.findOne({ _id: buildingId, "samples.sampleName": sampleName });

		if (sample === null) {
			const updateBuilding = await Building.findOneAndUpdate(
				{ _id: buildingId },
				{ $push: { samples: { sampleName, description, location } } },
				{ new: true }
			)
			res.send(true);
		} else {
			res.send(false)
		}

	} catch (error) {
		console.error(error);
		res.send(false)
	}
	
}

module.exports.RemoveSample = async (req, res) => {
  try {
    const { sampleId, buildingId } = req.params;
    const sample = await Building.findOneAndUpdate(
      { _id: buildingId, "samples._id": sampleId },
      { $pull: { samples: { _id: sampleId } } },
      { new: true }
    );

    if (!sample) {
      res.status(404).send('Not found');
      return;
    }

    res.send(true); // Signal successful removal
  } catch (error) {
    console.error(error);
    res.status(500).send(false); // Indicate error
  }
};

module.exports.UpdateSample = async (req, res) => {
	try {
		const updated = { 
			sampleName: req.body.newSampleName,
			description: req.body.newDescription,
			location: req.body.newLocation 
		};
		const { sampleId, buildingId } = req.params;

		await Building.updateOne(
			{ _id: buildingId, "samples._id": sampleId },
			{ $set: { "samples.$.sampleName": updated.sampleName,
								"samples.$.description": updated.description,
								"samples.$.location": updated.location
					 } },
			{ new: true }
		)

			res.send(true);

	} catch(error) {
			console.error(error)
			res.send(false)
	}
}