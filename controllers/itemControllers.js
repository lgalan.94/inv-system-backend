const Item = require('../models/Items');
const Building = require('../models/admin/Buildings'); 

module.exports.AddNewItem = async (req, res) => {
	try {

		let buildingId = req.params.buildingId;
		const { itemName, description, quantity, location, category, categoryId, imageUrl, lowStockThreshold } = req.body;

		const updateBuilding = await Building.findOneAndUpdate(
			{ _id: buildingId },
			{ $push: { items: { itemName, description, quantity, location, category, categoryId, imageUrl, lowStockThreshold } } },
			{ new: true }
		)
		res.send(true);

	} catch (error) {
		console.error(error);
		res.send(false)
	}
	
}


module.exports.AddItems = (req, res) => {
  	const items = req.body.map(item => new Item ({
  	  name: item.name.toUpperCase(),
  	  description: item.description,
  	  quantity: item.quantity,
  	  location: item.location,
  	  category: item.category,
  	  image_url: item.image_url,
  	  low_stock_threshold: item.low_stock_threshold
  	}));

  	Item.insertMany(items)
  	  .then(saved => res.send(true))
  	  .catch(err => res.send(false));
}

module.exports.RetrieveItems = (req, res) => {
			Item.find({})
			.then(response => {
						if (response.length > 0) {
								res.send(response)
						} else {
							res.send(false)
						}
			})
			.catch(error => res.send(error))
}

module.exports.GetItemById = (req, res) => {
			Item.findById(req.params.itemId)
			.then(result => res.send(result))
			.catch(error => res.send(error))
}

module.exports.RemoveItem = async (req, res) => {
  try {
    const { itemId, buildingId } = req.params;
    const item = await Building.findOneAndUpdate(
      { _id: buildingId, "items._id": itemId },
      { $pull: { items: { _id: itemId } } },
      { new: true }
    );

    if (!item) {
      res.status(404).send('Item not found');
      return;
    }

    res.send(true); // Signal successful removal
  } catch (error) {
    console.error(error);
    res.status(500).send(false); // Indicate error
  }
};

module.exports.UpdateItem = async (req, res) => {

	try {
		const { 
			newItemName, 
			newDescription, 
			newQuantity, 
			newLocation, 
			newCategory, 
			newImageUrl, 
			newLowStockThreshold } = req.body;
		let updatedItem = {
				itemName: newItemName,
				description: newDescription,
				quantity: newQuantity,
				location: newLocation,
				category: newCategory,
				imageUrl: newImageUrl,
				lowStockThreshold: newLowStockThreshold
		}
		const { itemId, buildingId } = req.params;

		await Building.updateOne(
			{ _id: buildingId, "items._id": itemId },
			{ $set: { "items.$.itemName": updatedItem.itemName,
								"items.$.description": updatedItem.description,
								"items.$.quantity": updatedItem.quantity,
								"items.$.location": updatedItem.location,
								"items.$.category": updatedItem.category,
								"items.$.imageUrl": updatedItem.imageUrl,
								"items.$.lowStockThreshold": updatedItem.lowStockThreshold
			 } },
			{ new: true }
		)

			res.send(true);

	} catch(error) {
			console.error(error)
			res.send(false)
	}
}