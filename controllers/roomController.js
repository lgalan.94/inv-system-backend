const Building = require('../models/admin/Buildings');

module.exports.AddRoom = async (req, res) => {
	try {

		let buildingId = req.params.buildingId;
		let { roomName } = req.body;

		const room = await Building.findOne({ _id: buildingId, "rooms.roomName": roomName });

		if (room === null) {
			const updateBuilding = await Building.findOneAndUpdate(
				{ _id: buildingId },
				{ $push: { rooms: { roomName } } },
				{ new: true }
			)
			res.send(true);
		} else {
			res.send(false)
		}

	} catch (error) {
		console.error(error);
		res.send(false)
	}
	
}

module.exports.RemoveRoom = async (req, res) => {
  try {
    const { roomId, buildingId } = req.params;
    const room = await Building.findOneAndUpdate(
      { _id: buildingId, "rooms._id": roomId },
      { $pull: { rooms: { _id: roomId } } },
      { new: true }
    );

    if (!room) {
      res.status(404).send('Room not found');
      return;
    }

    res.send(true); // Signal successful removal
  } catch (error) {
    console.error(error);
    res.status(500).send(false); // Indicate error
  }
};

module.exports.UpdateRoom = async (req, res) => {

	try {
		const updatedRoom = { roomName: req.body.newRoomName };
		const { roomId, buildingId } = req.params;

		await Building.updateOne(
			{ _id: buildingId, "rooms._id": roomId },
			{ $set: { "rooms.$.roomName": updatedRoom.roomName } },
			{ new: true }
		)

			res.send(true);

	} catch(error) {
			console.error(error)
			res.send(false)
	}
}