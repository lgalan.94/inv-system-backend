const Building = require('../models/admin/Buildings');

// Controller to create a new experiment in a building
module.exports.createExperiment = async (req, res) => {
  try {

    let buildingId = req.params.buildingId;
    let { title, description, status, results, researcher } = req.body;

    const experiment = await Building.findOne({ _id: buildingId, "experiments.title": title });

    if (experiment === null) {
      const updateBuilding = await Building.findOneAndUpdate(
        { _id: buildingId },
        { $push: { experiments: { title, description, status, results, researcher } } },
        { new: true }
      )
      res.send(true);
    } else {
      res.send(false)
    }

  } catch (error) {
    console.error(error);
    res.send(false)
  }
  
}


// Controller to update an experiment in a building
exports.updateExperiment = async (req, res) => {
  const buildingId = req.params.buildingId;
  const experimentId = req.params.experimentId;

  try {
    const building = await Building.findById(buildingId);

    if (!building) {
      return res.status(404).json({ error: 'Building not found' });
    }

    const experimentToUpdate = building.experiments.id(experimentId);

    if (!experimentToUpdate) {
      return res.status(404).json({ error: 'Experiment not found' });
    }

    // Assuming the request body contains the updated experiment details
    experimentToUpdate.set(req.body);

    await building.save();

    res.status(200).json(experimentToUpdate);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Controller to delete an experiment in a building
exports.deleteExperiment = async (req, res) => {
  const buildingId = req.params.buildingId;
  const experimentId = req.params.experimentId;

  try {
    const building = await Building.findById(buildingId);

    if (!building) {
      return res.status(404).json({ error: 'Building not found' });
    }

    building.experiments.id(experimentId).remove();
    await building.save();

    res.status(204).send(); // No content on successful deletion
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
