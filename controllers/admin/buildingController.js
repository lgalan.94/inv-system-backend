const Building = require('../../models/admin/Buildings');
const Users = require('../../models/Users');
const auth = require('../../auth.js');

module.exports.AddBuilding = (req, res) => {

	let { name, imageUrl, address, usersList, categories } = req.body;
	
	Building.findOne({ name: name })
	.then(response => {
		if (response === null) {
			let newBuilding = new Building({
				name: name,
				imageUrl: imageUrl,
				address: address,
				usersList: usersList,
				categories: categories
			})
			newBuilding.save()
			.then(saved => res.send(true))
			.catch(error => res.send(false))
		} else {
			 res.send(response)
		}
	})
	.catch(error => res.send(false))
}

// module.exports.AddUser = async (request, response) => {
//   try {
//     const buildingId = request.params.buildingId;
//     const { userId, name, email, imageUrl } = request.body;
//     const { buildingName, buildingImage, buildingAddress } = request.body;

//     const existingUser = await Users.findOne({ _id: userId });

//     if (!existingUser) {
//       return response.send(false); // User doesn't exist
//     }

//     const existingAssignment = await Building.findOne({ _id: buildingId, "usersList.userId": userId });

//     if (existingAssignment) {
//       return response.send(false); // User is already assigned to this building
//     }

//     // **Fix 1: Use findOneAndUpdate for atomic updates:**
//     const building = await Building.findOneAndUpdate(
//       { _id: buildingId },
//       { $push: { usersList: { userId, name, email, imageUrl } } },
//       { new: true } // Return the updated document
//     );

//     if (!building) {
//       return response.send(false); // Building not found
//     }

//     const user = await Users.findOneAndUpdate(
//       { _id: userId },
//       { $push: { buildingsAssigned: { buildingId, buildingName, buildingImage, buildingAddress } } },
//       { new: true }
//     );

//     response.send(true); // User added successfully
//   } catch (error) {
//     console.error(error); // Log the error for debugging
//     response.send(false);
//   }
// };



// module.exports.AddUser = (request, response) => {
// 		let buildingId = request.params.buildingId;
// 		let { userId, name, email, imageUrl } = request.body;
// 		let { buildingName, buildingImage, buildingAddress } = request.body;

// 		Users.findOne({ _id: userId })
// 		.then(result => {
// 			if (result !== null) {
// 					Building.findOne({ "usersList.userId": userId })
// 					.then(resp => {
// 						if (resp !== null) {
// 							return response.send(resp)
// 						} else {
// 							let isBuildingUpdated = Building.findOne({ _id: buildingId })
// 							.then(result => {
// 									result.usersList.push({
// 										userId: userId,
// 										name: name,
// 										email: email,
// 										imageUrl: imageUrl
// 									})
// 									result.save()
// 									.then(saved => true)
// 									.catch(error => false)

// 							})
// 							.catch(error => false)

// 							let isUserUpdated = Users.findOne({ _id: userId })
// 							.then(result => {
// 									result.buildingsAssigned.push({
// 										buildingId: buildingId,
// 										buildingName: buildingName,
// 										buildingImage: buildingImage,
// 										buildingAddress: buildingAddress
// 									})
// 									result.save()
// 									.then(saved => true)
// 									.catch(error => false)
// 							})
// 							.catch(error => false)

// 							if (isBuildingUpdated && isUserUpdated) {
// 									return response.send(true)
// 							} else {
// 									return response.send(false)
// 							}
// 						}
// 					})
// 					.catch(error => response.send(false))
// 			} else {
// 				return response.send(false);
// 			}
// 		})
// 		.catch(error => response.send(false))

// }

module.exports.AddUser = async (request, response) => {
  try {
    const buildingId = request.params.buildingId;
    const { userId, name, email, imageUrl } = request.body;
    const { buildingName, buildingImage, buildingAddress } = request.body;

    const existingUser = await Users.findOne({ _id: userId });

    if (existingUser) {
      const existingBuildingAssignment = await Building.findOne({ "usersList.userId": userId });

      if (existingBuildingAssignment) {
        return response.send(existingBuildingAssignment);
      } else {
        const building = await Building.findOneAndUpdate(
          { _id: buildingId },
          { $push: { usersList: { userId, name, email, imageUrl } } },
          { new: true }
        );

        const user = await Users.findOneAndUpdate(
          { _id: userId },
          { $push: { buildingsAssigned: { buildingId, buildingName, buildingImage, buildingAddress } } },
          { new: true }
        );

        return response.send(true);
      }
    } else {
      return response.send(false);
    }
  } catch (error) {
    console.error(error);
    return response.send(false);
  }
};



module.exports.RetrieveBuildings = (req, res) => {
		Building.find({})
		.then(response => {
				if (response.length !== 0) {
						res.send(response)
				} else {
						res.send(false)
				}
		})
}

// module.exports.RetrieveBuildingData = async (req, res) => {

//   try {
//     const { userId } = req.body;
//     const buildingId = req.params.buildingId;

//     const existingUser = await Building.findOne({ _id: buildingId, "usersList.userId" : userId });

//     console.log(existingUser)

//     if (existingUser) {
//       const result = await Building.findById(buildingId);
//       res.send(result)
//     } else {
//       res.send(false);
//     }
//   } catch (error) {
//       console.error(error);
//       res.send(false)
//   }
// }

module.exports.RetrieveBuildingData = (req, res) => {
    Building.findById(req.params.buildingId)
    .then(result => res.send(result))
    .catch(error => res.send(false))
}

module.exports.RemoveUser = async (req, res) => {
    try {
      const buildingId = req.params.buildingId;
      const { userId } = req.body;

     const existingUser = await Building.findOne({ _id: buildingId, "usersList.userId": userId });

     if (existingUser) {
       const result = await Building.findByIdAndUpdate(
          { _id: buildingId },
          { $pull: { usersList: { userId } } }
        );

       const updatedUsers = await Users.findOneAndUpdate(
         { _id: userId, "buildingsAssigned.buildingId": buildingId },
         { $pull: { buildingsAssigned: { buildingId } } }
       );

       res.send(true)
       
     } else {
        res.send(false)
     }

    }  catch (error){
        console.error(error);
        res.send(false);
    }
}

module.exports.UpdateBuildingData = async (req, res) => {
  try {
    const buildingId = req.params.buildingId;
    const updatedBuildingData = {
      name: req.body.newName,
  		imageUrl: req.body.newImageUrl,
  		address: req.body.newAddress
    };

    // Update building in Buildings collection
    const building = await Building.findByIdAndUpdate(buildingId, updatedBuildingData, { new: true });

    if (!building) {
      return res.status(404).send(false); // building not found
    }

    // Update user in buildings' usersList
    const updatedBuildingAssignments = await Users.updateMany(
      { "buildingsAssigned.buildingId": buildingId },
      { $set: { "buildingsAssigned.$.buildingName": updatedBuildingData.name, "buildingsAssigned.$.buildingImage": updatedBuildingData.imageUrl, "buildingsAssigned.$.buildingAddress": updatedBuildingData.address } }
    );

    res.send(true);
  } catch (error) {
    console.error(error);
    res.status(500).send(false); // Internal server error
  }
};


module.exports.DeleteBuildingData = async (req, res) => {
  try {
    const buildingId = req.params.buildingId;

    await Building.findByIdAndDelete(buildingId);

    // Remove user from buildings' usersList using $pull
    const updatedUsers = await Users.updateMany(
      { "buildingsAssigned.buildingId": buildingId },
      { $pull: { buildingsAssigned: { buildingId } } }
    );

    res.send(true);
  } catch (error) {
    console.error(error);
    res.status(500).send(false); // Internal server error
  }
};
