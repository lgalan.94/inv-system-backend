const mongoose = require('mongoose');

const categoriesSchema = new mongoose.Schema({
	buildingID: {
		type: String,
		required: true
	},
	categoryName: {
		type: String,
		required: true
	},
	imageUrl: {
		type: String,
		required: true
	},
	createdOn: {
		type: Date, default: new Date
	},
	isActive: {
		type: Boolean, default: true
	}
})

const Category = new mongoose.model('Category', categoriesSchema);

module.exports = Category