const mongoose = require('mongoose');

const buildingSchema = new mongoose.Schema({
	name: { type: String, required: true },
	imageUrl: { type: String, required: true },
	address: { type: String, required: true },
	createdOn: { type: Date, default: () => new Date() },
	isActive: { type: Boolean, default: true },
	usersList: [
		{
			userId: { type: String, required: [true, "userId is required!"]},
			userRole: Number,
			name: String,
			email: String,
			imageUrl: String,
			dateAdded: { type: Date, default: () => new Date() }
		}
	],
	categories: [
			{ 	
				categoryName: { type: String, required: true },
				imageUrl: { type: String, required: true },
				dateAdded: { type: Date, default: () => new Date() },
				isActive: { type: Boolean, default: true },
				createdOn: { type: Date, default: () => new Date() }
			}
	],
	items: [
			{
				itemName: { type: String, required: true },
				description: { type: String, required: true },
				quantity: { type: Number, required: true },
				location: { type: String, required: true },
				category: { type: String, required: true },
				categoryId: { type: String, required: true },
				lowStockThreshold: { type: Number, required: true },
				imageUrl: { type:  String, required: true },
				createdOn: { type: Date, default: () => new Date() }
			}
	],
	rooms: [
			{
				roomName: { type: String, required: true },
				createdOn: { type: Date, default: () => new Date() },
				isActive: { type: Boolean, default: true }
			}
	],
	samples: [
		{
			sampleName: {
			  type: String,
			  required: true,
			},
			description: String,
			location: String,
			createdOn: { type: Date, default: () => new Date() },
		}
	],
	samplesStorage: [
			{
				storageName: { type: String, required: true },
				createdOn: { type: Date, default: () => new Date() },
				isActive: { type: Boolean, default: true }
			}
	],
	experiments: [
		{
			title: {
			  type: String,
			  required: true,
			},
			description: {
			  type: String,
			  required: true,
			},
			startDate: {
			  type: Date,
			  default: Date.now,
			},
			endDate: {
			  type: Date,
			},
			status: {
			  type: String,
			  enum: ['Planning', 'InProgress', 'Completed'],
			  default: 'Planning',
			},
			results: {
			  type: String,
			},
			researcher: {
				type: String
			},
			// equipmentsUsed: [
			// 		{
			// 			itemId: String,
			// 			itemName: String
			// 		}
			// 	]
		}
	]


})

const Building = new mongoose.model("Building", buildingSchema);

module.exports = Building;