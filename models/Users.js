const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	imageUrl: {
		type: String
	},
	password: {
		type: String,
		required: true
	},
	createdOn: {
		type: Date, default: new Date()
	},
	userRole: {
		type: Number, default: 1
	},
	isActive: {
		type: Boolean, default: true
	},
	buildingsAssigned: [
		{
			buildingId: { type: String, required: [true, 'buildingId is required!']},
			buildingName: String,
			buildingImage: String,
			buildingAddress: String,
			createdOn: {
				type: Date, default: () => new Date()
			}
		}
	]

})

const Users = new mongoose.model('User', usersSchema);

module.exports = Users;