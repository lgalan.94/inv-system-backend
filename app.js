require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const itemRoutes = require('./routers/itemRoutes');
const categoryRoutes = require('./routers/categoryRoutes');
const userRoutes = require('./routers/userRoutes');
const settingRoutes = require('./routers/settingRoutes');
const buildingRoutes = require('./routers/admin/buildingRoutes');
const roomRoutes = require('./routers/roomRoutes');
const sampleRoutes = require('./routers/sampleRoutes');
const sampleStorageRoutes = require('./routers/sampleStorageRoutes');
const experimentRoutes = require('./routers/experimentRoutes');

const app = express();
const port = process.env.PORT || 5000;

mongoose.connect(process.env.DATABASE_URL);
const DBconn = mongoose.connection;
DBconn.on("error", console.error.bind(console, "Error database connection"));
DBconn.once("open", () => console.log('Successfully connnected to cloud database'));

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());
app.use('/ulafis', itemRoutes);
app.use('/ulafis/category', categoryRoutes);
app.use('/ulafis/room', roomRoutes);
app.use('/ulafis/sample', sampleRoutes);
app.use('/ulafis/storage', sampleStorageRoutes);
app.use('/ulafis/experiment', experimentRoutes);
app.use('/ulafis/user', userRoutes);
app.use('/ulafis/settings', settingRoutes);

app.use('/admin/building', buildingRoutes);



app.listen(port, () => console.log(`Server is running on port ${port}`))