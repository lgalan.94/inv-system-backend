const express = require('express');
const SampleController = require('../controllers/sampleController');

const router = express.Router();

router.post('/:buildingId/add-sample', SampleController.AddSample);
router.patch('/:buildingId/remove/:sampleId', SampleController.RemoveSample);
router.patch('/:buildingId/update/:sampleId', SampleController.UpdateSample)

module.exports = router;