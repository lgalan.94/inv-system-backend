const express = require('express');
const buildingController = require('../../controllers/admin/buildingController');
const router = express.Router();
const auth = require('../../auth.js');

router.post('/add', buildingController.AddBuilding);
router.get('/', buildingController.RetrieveBuildings);

router.get('/:buildingId', buildingController.RetrieveBuildingData);
router.post('/add-user/:buildingId', buildingController.AddUser);
router.delete('/:buildingId', buildingController.DeleteBuildingData);
router.patch('/remove-user/:buildingId', buildingController.RemoveUser);
router.patch('/:buildingId', buildingController.UpdateBuildingData);

module.exports = router;
