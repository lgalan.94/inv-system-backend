const express = require('express');
const SampleStorageController = require('../controllers/sampleStorageController');

const router = express.Router();

router.post('/:buildingId/add-storage', SampleStorageController.AddStorage);
router.patch('/:buildingId/remove/:storageId', SampleStorageController.RemoveStorage);
router.patch('/:buildingId/update/:storageId', SampleStorageController.UpdateStorage)

module.exports = router;