const express = require('express');
const userControllers = require('../controllers/userControllers');
const auth = require('../auth.js');

const router = express.Router();

router.post('/login', userControllers.Login);
router.post('/register', userControllers.RegisterUser);
router.get('/details', auth.verify, userControllers.retrieveUserDetails);
router.get('/list', userControllers.retrieveUsersList);

router.get('/user-details/:userId', userControllers.retrieveUser);
router.get('/details/:userId', auth.verify, userControllers.GetUserDetails);
router.patch('/:userId', userControllers.UpdateUser);
router.delete('/:userId', userControllers.DeleteUser);
router.patch('/user-status/:userId', userControllers.SetStatus);
router.patch('/user-role/:userId', userControllers.ChangeUserRole);

module.exports = router;