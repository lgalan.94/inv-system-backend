const express = require('express');
const categoryControllers = require('../controllers/categoryControllers');

const router = express.Router();

router.get('/:buildingId/all', categoryControllers.RetrieveCategories);
router.post('/:buildingId/add-category', categoryControllers.AddCategory);
router.patch('/:buildingId/remove/:categoryId', categoryControllers.RemoveCategory);
router.patch('/:buildingId/update/:categoryId', categoryControllers.UpdateCategory);

module.exports = router;