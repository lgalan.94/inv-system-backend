const express = require('express');
const roomController = require('../controllers/roomController');

const router = express.Router();

router.post('/:buildingId/add-room', roomController.AddRoom);
router.patch('/:buildingId/remove/:roomId', roomController.RemoveRoom);
router.patch('/:buildingId/update/:roomId', roomController.UpdateRoom)

module.exports = router;