const express = require('express');
const itemControllers = require('../controllers/itemControllers');

const router = express.Router();

router.post('/:buildingId/add-item', itemControllers.AddNewItem);
router.get('/items/all',  itemControllers.RetrieveItems);

router.get('/items/:itemId', itemControllers.GetItemById)
router.patch('/:buildingId/update/:itemId', itemControllers.UpdateItem)
router.patch('/:buildingId/remove/:itemId', itemControllers.RemoveItem)


module.exports = router;