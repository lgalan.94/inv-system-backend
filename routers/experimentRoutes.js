const express = require('express');
const ExperimentController = require('../controllers/experimentController');

const router = express.Router();

router.post('/:buildingId/create-experiment', ExperimentController.createExperiment);
// router.patch('/:buildingId/remove/:sampleId', ExperimentController.RemoveSample);
// router.patch('/:buildingId/update/:sampleId', SampleController.UpdateSample)

module.exports = router;